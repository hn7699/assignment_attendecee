package fi.vamk.e1800958.attendance;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class AttendanceDTO {
	
	private int id;
	private String key;
	private String day;
	
	public AttendanceDTO(int id, String key, String day) {
		super();
		this.id = id;
		this.key = key;
		this.day = day;
	}

	public String getDay() {
		return this.day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getKey() {
		return this.key;
	}

	public void setKey(String key) {
		this.key = key;
	}
	
	public Attendance convert()
	{
		try {
			Date date = new SimpleDateFormat("dd-MM-yyyy").parse(this.getDay());
			return new Attendance(this.getId(), this.getKey(), date);
		} catch (ParseException e) {
			Date date = null;
			return new Attendance(this.getId(), this.getKey(), date);
		}
		
		
	}

	public String toString() {
		return id + " " + key + " " + day;
	}

}
