package fi.vamk.e1800958.attendance;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;


@SpringBootApplication
public class SemdemoApplication {
    
    @Autowired 
	private AttendanceRepository repository;


	public static void main(String[] args) {
		SpringApplication.run(SemdemoApplication.class, args);
	}
	
	
	@Bean
	public void init() {
		AttendanceDTO a1= new AttendanceDTO(10,"a", "01-06-2010");
		AttendanceDTO a2= new AttendanceDTO(20,"b", "02-05-2007");
		AttendanceDTO a3= new AttendanceDTO(30,"c", "04-08-2003");
		repository.save(a1.convert());
		repository.save(a2.convert());
		repository.save(a3.convert());
	}


}
